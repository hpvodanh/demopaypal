<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="s" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath }/resources/css/style.css"
	rel="stylesheet" type="text/css">
</head>
<body>

	<h3>Product Details</h3>
	<form action="${pageContext.request.contextPath }/cart/update"
		method="post">
		
		
		<table border="1">
			<tr>
				<th>Action</th>
				<th>Id</th>
				<th>Name</th>
				<th>Photo</th>
				<th>Price</th>
				<th>Quantity <input type="submit" value="Update"></th>
				<th>Sub Total</th>
			</tr>

			<s:set var="total" value="0"></s:set>
			<s:forEach var="item" items="${sessionScope.cart }" varStatus="i">
				<s:set var="total"
					value="${total + item.product.price * item.quantity }"></s:set>
				<tr>
					<th align="center"><a
						href="${pageContext.request.contextPath }/cart/remove/${i.index}">Remove</a>
					</th>
					<th>${item.product.id }</th>
					<th>${item.product.name }</th>
					<th><img
						src="${pageContext.request.contextPath }/uploads/images/${item.product.photo }"
						width="120" height="100"></th>
					<th>${item.product.price }</th>
					<th><input type="number" name="quantity" min="1"
						value="${item.quantity }" style="width: 30px"></th>
					<th>	
						 ${item.product.price * item.quantity }
						 
						 
					</th>

				</tr>
			</s:forEach>
			<tr>
				<td colspan="6" align="right">Total</td>
				<td>${total }</td>
			</tr>

		</table>
	</form>
	<a href="${pageContext.request.contextPath }/product">Continue Shopping</a>
	<form action="${paypalConfig.posturl }" method="post">
	<!-- PayPal Setting --> 
		<input type="hidden" name="upload" value="1" /> 
		<input type="hidden" name="return" value="${paypalConfig.returnurl }" /> 
		<input type="hidden" name="cmd" value="_cart" /> 
		<input type="hidden" name="business" value="${paypalConfig.business}" />
		
		<s:forEach var="item" items="${sessionScope.cart }" varStatus="i">
			<input type="hidden" name="item_number_${i.index + 1 }" value="${item.product.id }" /> 
			<input type="hidden" name="item_name_${i.index + 1 }" value="${item.product.name} " /> 
			<input type="hidden" name="amount_${i.index + 1 }" value="${item.product.price} " /> 
			<input type="hidden" name="quantity_${i.index + 1 }" value="${item.quantity}" /> 
		</s:forEach>
		<input type="submit" value="Check out">
	</form>
</body>
</html>