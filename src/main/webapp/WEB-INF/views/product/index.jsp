<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>	
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath }/resources/css/style.css" 
	rel="stylesheet" type="text/css">
</head>
<body>
	
	<h3>Product List</h3>
	<table border="1">
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Photo</th>
			<th>Price</th>
			<th>Action</th>
		</tr>
		<c:forEach var="p" items="${products }">
			<tr>
				<td>${p.id }</td>
				<td>${p.name }</td>
				<td>
					<img src="${pageContext.request.contextPath }/uploads/images/${p.photo }" 
						width="120" height="100">
				</td>
				<td>${p.price }</td>
				<td>
					<a href="${pageContext.request.contextPath }/cart/buy/${p.id }">Buy now</a>
				</td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>