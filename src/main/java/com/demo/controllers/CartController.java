package com.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.models.Item;
import com.demo.models.Product;
import com.demo.paypal.PayPalResult;
import com.demo.paypal.PayPalSucess;
import com.demo.rervices.PaypalServices;
import com.demo.rervices.ProductServices;


@Controller
@RequestMapping("cart")
public class CartController {

	
	@Autowired
	private ProductServices productServices;
	
	@Autowired
	private PaypalServices paypalServices;
	
	@RequestMapping(value = "index",method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		modelMap.put("paypalConfig", paypalServices.gePayPalConfig());
		return "cart/index";
	}
	
	@RequestMapping(value = "buy/{id}",method = RequestMethod.GET)
	public String buy(@PathVariable("id") int id, HttpSession session) {
		Product product = productServices.find(id);
		if(session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<Item>();
			cart.add(new Item(product, 1));
			session.setAttribute("cart", cart);
		}else {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			int index = exists(id, cart);
			if(index == -1) {
				cart.add(new Item(product, 1));
			}else {
				int newQuantity = cart.get(index).getQuantity() + 1;
				cart.get(index).setQuantity(newQuantity);
			}
		}
		return "redirect:/cart/index";
	}
	
	@RequestMapping(value = "remove/{index}",method = RequestMethod.GET)
	public String remove(@PathVariable("index") int index, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		cart.remove(index);
		session.setAttribute("cart", cart);
		return "redirect:/cart/index";
	}
	
	@RequestMapping(value = "update",method = RequestMethod.POST)
	public String update(@RequestParam("quantity") int[] quantities, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		for(int i = 0; i < cart.size(); i++) {
			cart.get(i).setQuantity(quantities[i]);
		}
		return "redirect:/cart/index";
	}
	
	@RequestMapping(value = "success",method = RequestMethod.GET)
	public String success(HttpServletRequest request, HttpSession session) {
		PayPalSucess payPalSucess = new PayPalSucess();
		PayPalResult payPalResult = payPalSucess.getPayPal(request, paypalServices.gePayPalConfig());
		System.out.println("Transaction Info");
		System.out.println("City " + payPalResult.getAddress_city());
		/*Create new order*/
		/*Create order details*/
		/*Destroy cart*/
		session.removeAttribute("cart");
		return "cart/success";
	}
	
	private int exists(int id, List<Item> cart) {
		for(int i = 0; i < cart.size(); i++) {
			if(cart.get(i).getProduct().getId() == id) {
				return i;
			}
		}
		return -1;
	}
}
