package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.InvoiceDetails;
import com.demo.models.InvoiceDetailsId;

@Repository("InvoiceDetailsRepository")
public interface InvoiceDetailsRepository extends CrudRepository<InvoiceDetails, InvoiceDetailsId>{

}
