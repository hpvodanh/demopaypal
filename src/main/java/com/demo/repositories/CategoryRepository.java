package com.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.models.Category;

@Repository("CategoryRepository")
public interface CategoryRepository extends CrudRepository<Category, Integer>{

}
