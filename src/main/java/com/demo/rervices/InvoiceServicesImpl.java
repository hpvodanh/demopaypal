package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.models.Invoice;
import com.demo.repositories.InvoiceRepository;

public class InvoiceServicesImpl implements InvoiceServices{

	@Autowired
	private InvoiceRepository invoiceRepository;

	@Override
	public Iterable<Invoice> findAll() {
		return invoiceRepository.findAll();
	}

	@Override
	public Invoice find(int id) {
		return invoiceRepository.findById(id).get();
	}

	@Override
	public Invoice save(Invoice invoice) {
		return invoiceRepository.save(invoice);
	}

	@Override
	public void delete(int id) {
		invoiceRepository.deleteById(id);
	}
}
