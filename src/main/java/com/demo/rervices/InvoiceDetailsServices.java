package com.demo.rervices;


import com.demo.models.InvoiceDetails;
import com.demo.models.InvoiceDetailsId;

public interface InvoiceDetailsServices {

	public Iterable<InvoiceDetails> findAll();
	public InvoiceDetails find(InvoiceDetailsId id);
	public InvoiceDetails save(InvoiceDetails invoiceDetails);
	public void delete(InvoiceDetailsId id);
}
