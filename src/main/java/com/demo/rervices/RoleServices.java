package com.demo.rervices;

import com.demo.models.Role;

public interface RoleServices {
	public Iterable<Role> findAll();
	public Role find(int id);
	public Role save(Role role);
	public void delete(int id);
}
