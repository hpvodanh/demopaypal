package com.demo.rervices;

import com.demo.models.Account;

public interface AccountServices {

	public Iterable<Account> findAll();
	public Account find(int id);
	public Account save(Account account);
	public void delete(int id);
}
