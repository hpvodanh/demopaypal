package com.demo.rervices;


import com.demo.models.Category;

public interface CategoryServices {

	public Iterable<Category> findAll();
	public Category find(int id);
	public Category save(Category category);
	public void delete(int id);
}
