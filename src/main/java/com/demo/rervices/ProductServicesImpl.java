package com.demo.rervices;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Product;
import com.demo.repositories.ProductRepository;

@Service("productServices")
public class ProductServicesImpl implements ProductServices{

	@Autowired
	private ProductRepository productRepository;

	@Override
	public Iterable<Product> findAll() {
		return productRepository.findAll();
	}

	@Override
	public Product find(int id) {
		return productRepository.findById(id).get();
	}

	@Override
	public Product save(Product product) {
		return productRepository.save(product);
	}

	@Override
	public void delete(int id) {
		productRepository.deleteById(id);
		
	}

	@Override
	public List<Product> findFeatured(boolean featured, int n) {
		
		return productRepository.findByFeaturedProduct(featured, n);
	}

	@Override
	public List<Product> latest(int n) {
		
		return productRepository.latest(n);
	}
}
