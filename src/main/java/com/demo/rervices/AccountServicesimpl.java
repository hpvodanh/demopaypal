package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.models.Account;
import com.demo.repositories.AccountRepository;

@Service("accountServices")
public class AccountServicesimpl implements AccountServices{

	@Autowired
	private AccountRepository accountRepository;

	@Override
	public Iterable<Account> findAll() {
		return accountRepository.findAll();
	}

	@Override
	public Account find(int id) {

		return accountRepository.findById(id).get();
	}

	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public void delete(int id) {
		accountRepository.deleteById(id);
	}
}
