package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.models.Role;
import com.demo.repositories.RoleRepository;

public class RoleServicesImpl implements RoleServices{

	@Autowired
	private RoleRepository roleRepository;

	@Override
	public Iterable<Role> findAll() {
		return roleRepository.findAll();
	}

	@Override
	public Role find(int id) {
		return roleRepository.findById(id).get();
	}

	@Override
	public Role save(Role role) {
		return roleRepository.save(role);
	}

	@Override
	public void delete(int id) {
		roleRepository.deleteById(id);
		
	}
}
