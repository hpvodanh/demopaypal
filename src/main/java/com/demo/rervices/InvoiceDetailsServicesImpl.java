package com.demo.rervices;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.models.InvoiceDetails;
import com.demo.models.InvoiceDetailsId;
import com.demo.repositories.InvoiceDetailsRepository;

public class InvoiceDetailsServicesImpl implements InvoiceDetailsServices{

	@Autowired
	private InvoiceDetailsRepository invoiceDetailsRepository;

	@Override
	public Iterable<InvoiceDetails> findAll() {
		return invoiceDetailsRepository.findAll();
	}

	@Override
	public InvoiceDetails find(InvoiceDetailsId id) {
		return invoiceDetailsRepository.findById(id).get();
	}

	@Override
	public InvoiceDetails save(InvoiceDetails invoiceDetails) {
		return invoiceDetailsRepository.save(invoiceDetails);
	}

	@Override
	public void delete(InvoiceDetailsId id) {

		invoiceDetailsRepository.deleteById(id);
		
	}



	
}
