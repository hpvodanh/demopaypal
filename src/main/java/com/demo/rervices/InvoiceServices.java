package com.demo.rervices;

import com.demo.models.Invoice;

public interface InvoiceServices {

	public Iterable<Invoice> findAll();
	public Invoice find(int id);
	public Invoice save(Invoice invoice);
	public void delete(int id);
}
