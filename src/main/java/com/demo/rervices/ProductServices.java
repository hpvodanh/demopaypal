package com.demo.rervices;

import java.util.List;


import com.demo.models.Product;

public interface ProductServices {

	public Iterable<Product> findAll();
	public Product find(int id);
	public Product save(Product product);
	public void delete(int id);
	public List<Product> findFeatured(boolean featured, int n);
	public List<Product> latest(int n);
}
